import React, { Component } from 'react';
import { connect } from 'react-redux';
import { todos } from '../reduxion';

const ENTER_KEY = 13;
const ESC_KEY = 27;

@connect()
export default class Todo extends Component {
  state = {
    editing: false,
  }

  handleDestroy = () => {
    todos.remove(v => this.props.todo === v);
  }

  handleComplete = () => {
    todos.toggleItems(v => this.props.todo === v);
  }

  handleKeyPress = (e) => {
    const value = e.target.value;
    if (e.which === ENTER_KEY) {
      todos.editItems(v => this.props.todo === v, value);
      this.setState({ editing: false });
    }
  }

  handleKeyDown = (e) => {
    if (e.which === ESC_KEY) {
      this.setState({ editing: false });
    }
  }

  startEditing = () => {
    this.setState({ editing: true });
  }

  handleBlur = (e) => {
    const value = e.target.value;
    todos.editItems(v => this.props.todo === v, value);
    this.setState({ editing: false });
  }

  className() {
    if (this.state.editing) return 'editing';
    if (this.props.todo.completed) return 'completed';
    return undefined;
  }

  render() {
    const { todo } = this.props;
    const { editing } = this.state;

    return (
      <li className={this.className()}>
        <div className="view">
          <input
            className="toggle"
            type="checkbox"
            checked={todo.completed}
            onChange={this.handleComplete}
          />
          <label onDoubleClick={this.startEditing}>{todo.text}</label>
          <button className="destroy" onClick={this.handleDestroy} />
        </div>
        {
          editing &&
            <input
              ref={(c) => { this.input = c; }}
              className="edit"
              autoFocus
              onKeyPress={this.handleKeyPress}
              onKeyDown={this.handleKeyDown}
              defaultValue={todo.text}
              onBlur={this.handleBlur}
            />
        }
      </li>
    );
  }
}
