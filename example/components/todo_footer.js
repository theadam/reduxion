import React, { Component } from 'react';
import { connect } from 'react-redux';

import { todos as todosReduxion, filter } from '../reduxion';

@connect(state => ({ todos: state.todos, filter: state.filter }))
export default class TodoFooter extends Component {

  className(item) {
    if (this.props.filter.name === item) {
      return 'selected';
    }
    return undefined;
  }

  clearCompleted = () => {
    todosReduxion.clearCompleted();
  }

  changeFilter = name => () => {
    filter[name]();
  }

  render() {
    const { todos } = this.props;
    const activeLength = todos.filter(todo => !todo.completed).length;
    const s = activeLength !== 1 ? 's' : '';

    return (
      <footer className="footer">
        <span className="todo-count">
          <strong>{activeLength}</strong> item{s} left
        </span>
        <ul className="filters">
          <li>
            <a
              className={this.className('all')}
              href="#/"
              onClick={this.changeFilter('all')}
            >
              All
            </a>
          </li>
          <li>
            <a
              className={this.className('active')}
              href="#/active"
              onClick={this.changeFilter('active')}
            >
              Active
            </a>
          </li>
          <li>
            <a
              className={this.className('completed')}
              href="#/completed"
              onClick={this.changeFilter('completed')}
            >
              Completed
            </a>
          </li>
        </ul>
        <button
          className="clear-completed"
          onClick={this.clearCompleted}
        >
          Clear Completed
        </button>
      </footer>
    );
  }
}
