import React, { Component } from 'react';
import { connect } from 'react-redux';

import Todo from './todo';

@connect(state => ({ todos: state.todos, filter: state.filter }))
export default class TodoList extends Component {
  todos() {
    return this.props.todos
      .map((todo, i) => [todo, i])
      .filter(([todo, i]) => this.props.filter.fn(todo, i));
  }

  render() {
    return (
      <ul className="todo-list">
        {
          this.todos().map(([todo, i]) =>
            <Todo todo={todo} key={i} />
          )
        }
      </ul>
    );
  }
}
