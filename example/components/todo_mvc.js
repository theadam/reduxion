import React, { Component } from 'react';
import { connect } from 'react-redux';

import { todos } from '../reduxion';

import MainArea from './main_area';

const ENTER_KEY = 13;

@connect(state => ({ todos: state.todos }))
export default class TodoMvc extends Component {
  handleKeyPress = (e) => {
    const value = e.target.value;
    if (e.which === ENTER_KEY && e.target.value.trim()) {
      e.target.value = '';
      todos.create(value);
    }
  }

  render() {
    return (
      <div>
        <section className="todoapp">
          <header className="header">
            <h1>todos</h1>
            <input
              className="new-todo"
              placeholder="What needs to be done?"
              autoFocus
              onKeyPress={this.handleKeyPress}
            />
          </header>
          {
            this.props.todos.length > 0 &&
              <MainArea />
          }
        </section>
        <footer className="info">
          <p>Double-click to edit a todo</p>
          <p>Template by <a href="http://sindresorhus.com">Sindre Sorhus</a></p>
          <p>Created by <a href="http://todomvc.com">you</a></p>
          <p>Part of <a href="http://todomvc.com">TodoMVC</a></p>
        </footer>
      </div>
    );
  }
}
