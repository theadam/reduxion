import React, { Component } from 'react';
import { connect } from 'react-redux';

import TodoList from './todo_list';
import TodoFooter from './todo_footer';
import { todos } from '../reduxion';

@connect(state => ({ todos: state.todos }))
export default class MainArea extends Component {
  checked() {
    return this.props.todos.every(t => t.completed);
  }

  handleToggleAll = () => todos.toggleAll(!this.checked());

  render() {
    return (
      <section className="main">
        <input
          className="toggle-all"
          onChange={this.handleToggleAll}
          type="checkbox"
          checked={this.checked()}
        />
        <label htmlFor="toggle-all">Mark all as complete</label>
        <TodoList />
        <TodoFooter />
      </section>
    );
  }
}
