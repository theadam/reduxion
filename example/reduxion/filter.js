import { reduxion, action, match, description } from 'reduxion';

const all = { name: 'all', fn: () => true };
const completed = { name: 'completed', fn: v => v.completed };
const active = { name: 'active', fn: v => !v.completed };

const hash = {
  '#/': all,
  '#/active': active,
  '#/completed': completed,
};


@reduxion('Filter')
export default class Filter {
  initialState = hash[window.location.hash] || all

  @description('changes the filter') @action change

  all = () => this.change(all)

  completed = () => this.change(completed)

  active = () => this.change(active)

  reducer = match({
    [this.change]: (_, filter) => filter,
  });
}
