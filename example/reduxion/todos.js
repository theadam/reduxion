import { reduxion, action, listOf, locallyStore, match } from 'reduxion';

@reduxion()
class Todo {
  initialState = {
    text: '',
    completed: false,
  }

  @action edit;

  @action
  toggle(val) {
    if (!val) return current => !current;
    return () => val;
  }

  reducer = match({
    [this.edit]: (state, text) => ({ ...state, text }),
    [this.toggle]: (state, toggle) => ({ ...state, completed: toggle(state.completed) }),
  })
}

@reduxion('Todos')
@locallyStore('todos')
@listOf(Todo)
export default class Todos {
  initialState = []

  clearCompleted() {
    return this.remove(item => item.completed);
  }

  create(text) {
    return this.add({ text });
  }
}

