import { applyMiddleware, createStore, compose } from 'redux';
import { combine, reduxionEnhancer, combineToReduxion } from 'reduxion';
import { devMiddleware } from 'redux-devtools-preset';

import Todos from './reduxion/todos';
import Filter from './reduxion/filter';
import { element } from './element';

export const todos = new Todos();
export const filter = new Filter();

function logger({ getState }) {
  return next => (action) => {
    console.log('will dispatch', action);

    // Call the next dispatch method in the middleware chain.
    const returnValue = next(action);

    console.log('state after dispatch', getState());

    // This will likely be the action itself, unless
    // a middleware further in chain changed it.
    return returnValue;
  };
}

// regular reduxion
function lastAction(store, action) {
  return action;
}

const reduxion = combine({
  todos,
  filter,
  ...combineToReduxion({
    lastAction,
  }),
});

export const store = createStore(reduxion, compose(
  reduxionEnhancer,
  applyMiddleware(logger),
  ...devMiddleware({ rootNode: element }),
));

