import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';

import TodoMvc from './components/todo_mvc';
import { element } from './element';
import { store } from './reduxion';

render(
  <Provider store={store}>
    <TodoMvc />
  </Provider>,
  element,
);

