import { sym, storeOf, connect, displayName } from './symbols';

function wrapReduxion(Target, name = '[ReduxionClass]') {
  return class ReduxionWrapper extends Target {
    static toString = () => sym(Target, name);
    [displayName] = name;

    toString() {
      return sym(this, name, 'instance');
    }

    [connect](store) {
      storeOf(this, store);
    }

    reducer(state, action) {
      if (super.reducer) {
        return super.reducer(state, action);
      }
      return state;
    }

    effects(state, action, store) {
      if (super.effects) {
        return super.effects(state, action, store);
      }
      return undefined;
    }
  };
}

export function reduxion(arg) {
  if (arg === undefined) return target => wrapReduxion(target);
  if (typeof arg === 'string') return target => wrapReduxion(target, arg);
  return wrapReduxion(arg);
}

export function makeReduxion(reducer = state => state, effects = () => {}, connector = () => {}) {
  return {
    reducer,
    effects,
    [connect]: connector,
  };
}

