import { hasProp } from './utils';

export const connect = Symbol('connect');
export const displayName = Symbol('displayName');

const actionsSym = Symbol('actions');
export function actionsOf(target, value) {
  if (value !== undefined) target[actionsSym] = value;
  return target[actionsSym] || [];
}

const storeSym = Symbol('store');
export function storeOf(target, value) {
  if (value !== undefined) target[storeSym] = value;
  return target[storeSym] || [];
}

const symLoc = Symbol('Location of Symbol');
export function sym(target, name, type = 'reduxion') {
  if (!hasProp(target, symLoc)) {
    target[symLoc] = Symbol(`${name || target.name} ${type}`);
  }
  return target[symLoc];
}

