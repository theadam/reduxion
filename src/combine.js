import { connect } from './symbols';
import { mapObj } from './utils';

export function combineReducers(reducers) {
  const keys = Object.keys(reducers);
  if (keys.length === 0) return state => state;
  return keys.reduce((acc, key) =>
    function innerCombineReducers(state, ...args) {
      const reducer = reducers[key];
      const newState = acc(state, ...args);
      return { ...newState, [key]: reducer(newState ? newState[key] : undefined, ...args) };
    }, state => state);
}

export function combine(obj) {
  const reducers = mapObj(obj, v => v.reducer.bind(v));

  return {
    reducer: combineReducers(reducers),
    effects: (state, ...args) => Object.keys(obj).forEach(key =>
      obj[key].effects(state && state[key], ...args)),
    [connect]: store => Object.values(obj).forEach(val => val[connect](store)),
  };
}
