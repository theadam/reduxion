import { hasProp, err, of } from './utils';

function matchType(obj, state, action, store) {
  if (hasProp(obj, action.type)) {
    return obj[action.type].call(this, state, ...of(action.payload), store);
  }
  return state;
}

function matchAction(obj, state, action, store) {
  if (hasProp(obj, action.action)) {
    return obj[action.action].call(this, state, ...of(action.payload), store);
  }
  return state;
}

function matchInstance(obj, state, action, store) {
  if (hasProp(obj, action.instance)) {
    return obj[action.instance].call(this, state, action, store);
  }
  return state;
}

function matchSource(obj, state, action, store) {
  if (hasProp(obj, action.source)) {
    return obj[action.source].call(this, state, action, store);
  }
  return state;
}

function matchCatchAll(obj, state, action, store) {
  if (hasProp(obj, '_')) {
    return obj._.call(this, state, action, store);
  }
  return state;
}

function composeMatchers(...args) {
  if (args.length === 1) return args[0];
  return args.reduce((acc, v) =>
    function composedMatchers(obj, state, action, store) {
      const firstResult = v.call(this, obj, state, action, store);
      const newState = store !== undefined ? state : firstResult;
      return acc.call(this, obj, newState, action, store);
    }
  );
}

function catchErrors(obj) {
  Object.keys(obj).forEach((key) => {
    if (key === 'undefined') err('You passed undefined to a match');
  });
}

export function match(obj) {
  catchErrors(obj);
  return function innerMatch(state = this.initialState, action, store) {
    return composeMatchers(
      matchCatchAll,
      matchSource,
      matchType,
      matchInstance,
      matchAction,
    ).call(this, obj, state, action, store);
  };
}
