import { sym, actionsOf, storeOf, displayName } from './symbols';

export function description(message) {
  return function innerDescription(target, name, desc) {
    const targetClass = target.constructor;
    const oldCreator = targetClass[name];

    function newCreator(...args) {
      const { actionName, ...rest } = oldCreator(...args);
      return { actionName, description: message, ...rest };
    }

    newCreator.actionName = oldCreator.actionName;
    newCreator.toString = oldCreator.toString;

    targetClass[name] = newCreator;

    return desc;
  };
}

function makeAction(target, actionName, payloadMod = (...a) => a) {
  const type = Symbol(`${actionName} action`);
  const cons = function actionCreator(...args) {
    return {
      actionName,
      payload: payloadMod(...args),
      type: cons,
    };
  };
  cons.actionName = actionName;
  cons.toString = () => type;
  actionsOf(target, actionsOf(target).concat(cons));
  return cons;
}

export function forInstance(baseAction, instance) {
  return {
    className: instance[displayName] || '[ReduxionClass]',
    ...baseAction,
    instance,
    action: instance[baseAction.actionName],
    source: instance.constructor,
  };
}

function makeDispatcher({ actionName }) {
  const dispatcher = function innerDispatch(...a) {
    const act = this.constructor[actionName];
    return storeOf(this).dispatch(forInstance(act(...a), this));
  };
  dispatcher.toString = function toString() {
    return sym(this, actionName, 'action');
  };
  return dispatcher;
}

export function action(target, name, desc) {
  const { value, initializer, ...rest } = desc;
  const mapper = (value && (() => value)) || initializer || (() => undefined);

  const targetClass = target.constructor;
  const act = makeAction(targetClass, name, mapper());
  targetClass[name] = act;

  return {
    ...rest,
    value: makeDispatcher(act),
  };
}

export function actions(target, name, desc) {
  const { initializer, ...rest } = desc;
  const targetClass = target.constructor;

  initializer().forEach((aname) => {
    const act = makeAction(target, aname);
    targetClass[aname] = act;
    target[aname] = makeDispatcher(act);
  });

  return {
    ...rest,
    value: undefined,
  };
}
