import { match } from '../match';

export default function locallyStore(key) {
  return function innerLocallyStore(Target) {
    return class LocalStorageWrapper extends Target {
      constructor(...args) {
        super(...args);
        const fromLocalStorage = localStorage.getItem(key);
        this.initialState = fromLocalStorage ?
          JSON.parse(fromLocalStorage) :
          this.initialState;

        const superEffects = this.effects;
        this.effects = function effects(state, action) {
          superEffects(state, action);
          this.subEffects(state, action);
        };
      }

      subEffects = match({
        [this]: state => localStorage.setItem(key, JSON.stringify(state)),
      })
    };
  };
}