import { actionsOf } from '../symbols';
import { actions, forInstance } from '../action';
import { match } from '../match';
import { isObject } from '../utils';

export default function listOf(Base, key) {
  return function innerListOf(Target) {
    const baseActions = actionsOf(Base);
    return class ListOfWrapper extends Target {
      constructor(...args) {
        super(...args);
        this.base = new Base();

        if (key) {
          this.initialState = this.initialState || { [key]: [] };
          if (this.initialState[key] === undefined) this.initialState[key] = [];
          if (!isObject(this.initialState) || !Array.isArray(this.initialState[key])) {
            throw new Error(`Expected state to be an object with '${key}' key`);
          }
        } else {
          this.initialState = this.initialState || [];
          if (!Array.isArray(this.initialState)) {
            throw new Error('Expected state to be an array');
          }
        }

        const superReducer = this.reducer;
        const subReducer = match({
          [this.add](state, overrides = {}) {
            return state.concat({ ...this.base.initialState, ...overrides });
          },

          [this.remove](state, selector) {
            return state.filter((v, i) => !selector(v, i));
          },

          [this.modify](state, selector, innerAction) {
            return state.map((item, idx) => {
              if (!selector(item, idx)) return item;
              return this.base.reducer(item, forInstance(innerAction, this.base));
            });
          },
        });

        this.reducer = (state, action) => {
          let newState;
          if (key) {
            if (state[key] && !Array.isArray(state[key])) throw new Error(`Expected ${key} to be an array`);
            newState = { ...state, [key]: subReducer.call(this, state[key], action) };
          } else {
            if (state && !Array.isArray(state)) throw new Error('Expected state to be an array');
            newState = subReducer.call(this, state, action);
          }
          return superReducer.call(this, newState, action);
        };

        baseActions.forEach((baseAction) => {
          this[`${baseAction.actionName}Item`] = function item(i, ...a) {
            return this.modifyItem(i, baseAction(...a));
          };

          this[`${baseAction.actionName}Items`] = function items(selector, ...a) {
            return this.modifyItems(selector, baseAction(...a));
          };

          this[`${baseAction.actionName}All`] = function all(...a) {
            return this.modify(() => true, baseAction(...a));
          };
        });
      }

      @actions actions = ['add', 'remove', 'modify']

      removeItem(i) {
        return this.deleteItems((v, idx) => i === idx);
      }

      removeItems(...args) {
        return this.remove(...args);
      }

      modifyItems(...args) {
        return this.modify(...args);
      }

      modifyItem(i, ...args) {
        return this.modify((v, idx) => i === idx, ...args);
      }
    };
  };
}
