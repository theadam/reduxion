import { actions } from '../action';
import { match } from '../match';
import { isObject } from '../utils';

export default function attr(key) {
  const uKey = key[0].toUpperCase() + key.slice(1);
  const setKey = `set${uKey}`;
  const resetKey = `reset${uKey}`;
  return function innerAttr(Target) {
    return class AttrWrapper extends Target {
      constructor(...args) {
        super(...args);

        this.initialState = this.initialState || {};
        if (!isObject(this.initialState)) {
          throw new Error('Expected state to be object.');
        }

        const superReducer = this.reducer;
        const subReducer = match({
          [this[setKey]]: (state, value) => {
            if (isObject(state)) {
              return { ...state, [key]: value };
            }
            throw new Error(`Expected state to be an object with '${key}' key`);
          },
          [this[resetKey]]: (state) => {
            if (isObject(state)) {
              return { ...state, [key]: this.initialState[key] };
            }
            throw new Error(`Expected state to be an object with '${key}' key`);
          },
        });

        this.reducer = (state, action) => {
          const newState = subReducer.call(this, state, action);
          return superReducer.call(this, newState, action);
        };
      }

      @actions actions = [setKey, resetKey]
    };
  };
}
