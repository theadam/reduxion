export const hasProp = (target, prop) => Object.prototype.hasOwnProperty.call(target, prop);

export function err(message) {
  throw new Error(message);
}

export function of(ary) {
  return [].concat(ary);
}

export const mapObj = (obj, fn) =>
  Object.keys(obj).reduce((acc, key) => ({ ...acc, [key]: fn(obj[key]) }), {});

export function isObject(value) {
  return value === Object(value) && Object.prototype.toString.call(value) === '[object Object]';
}
