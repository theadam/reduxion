import { connect } from './symbols';
import { makeReduxion } from './reduxion';
import { mapObj } from './utils';

export { reduxion } from './reduxion';
export { match } from './match';
export { action, actions, description } from './action';
export { combine } from './combine';
export { listOf, attr, locallyStore } from './decorators';

export function reduxionEnhancer(createStore) {
  return function innerReduxionEnhancer(reduxion, ...args) {
    const store = createStore(reduxion.reducer.bind(reduxion), ...args);
    const enhanced = {
      ...store,
      dispatch(action) {
        const result = store.dispatch(action);
        reduxion.effects(store.getState(), action, store);
        return result;
      },
    };
    reduxion[connect](enhanced);
    return enhanced;
  };
}

export function toReduxion(reducer) {
  return makeReduxion(reducer);
}

export function combineToReduxion(reducers) {
  return mapObj(reducers, toReduxion);
}
